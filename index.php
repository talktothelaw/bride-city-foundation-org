<?php include('./includes/header.php') ?>
<body>
<?php $home = "current-menu-item"; ?>
<?php include('./includes/nav.php') ?>
<?php include('./top/home.php') ?>
<div class="home-page-icon-boxes">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-4 mt-4 mt-lg-0">
                <div class="icon-box active">
                    <figure class="d-flex justify-content-center">
                        <img src="images/hands-gray.png" alt="">
                        <img src="images/hands-white.png" alt="">
                    </figure>

                    <header class="entry-header">
                        <h4 class="entry-title">VOLUNTEER</h4>
                    </header>

                    <div class="entry-content">
                        <p>You could earn some stipend as a Volunteer Mobilizer with Bride City Cooperatives. Volunteer today. </p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 mt-4 mt-lg-0">
                <div class="icon-box">
                    <figure class="d-flex justify-content-center">
                        <img src="images/charity-gray.png" alt="">
                        <img src="images/charity-white.png" alt="">
                    </figure>

                    <header class="entry-header">
                        <h6 class="entry-title">GROW ORGANIC FOOD</h6>
                    </header>

                    <div class="entry-content">
                        <p>Organic fertilizers and eco-friendly plants are affordable in your neighbourhood. Let's connect you to one. </p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 mt-4 mt-lg-0">
                <div class="icon-box active">
                    <figure class="d-flex justify-content-center">
                        <img src="images/donation-gray.png" alt="">
                        <img src="images/donation-white.png" alt="">
                    </figure>

                    <header class="entry-header">
                        <h5 class="entry-title">GET GRANTS</h5>
                    </header>

                    <div class="entry-content">
                        <p>Attend our Capacity Building Seminars and join a Coop Credit Community to get a Grant. </p>
                    </div>
                </div>
            </div>
        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- .home-page-icon-boxes -->

<div class="home-page-welcome">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-6 order-2 order-lg-1">
                <div class="welcome-content">
                    <header class="entry-header">
                        <h2 class="entry-title">BRIDE CITY JOINS THE FIGHT!</h2>
                    </header><!-- .entry-header -->

                    <div class="entry-content mt-5">
                        <p> The COVID-19 Pandemic has halted and changed social economies and financial balances in the world. It kills young and old and afflicts the world with hunger by taking all livelihood from families and cutting our essential supply chains.

                        <p>We can't watch and do nothing. We are providing health kits and food supplies. We are rebuilding economic capacities for essential supply chains of most vulnerable persons.</p>
                        We need more partners and every penny counts.</p>
                    </div><!-- .entry-content -->

                    <div class="entry-footer mt-5">
                        <a href="https://wealth-hub.org/donation.php" class="btn gradient-bg mr-2"> Join us today</a>
                    </div><!-- .entry-footer -->
                </div><!-- .welcome-content -->
            </div><!-- .col -->

            <div class="col-12 col-lg-6 mt-4 order-1 order-lg-2">
                <img src="images/coro.jpeg" alt="welcome" style="opacity: 0.7;">
            </div><!-- .col -->
        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- .home-page-icon-boxes -->

<div class="home-page-events">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="upcoming-events">
                    <div class="section-heading">
                        <h2 class="entry-title">Events</h2>
                    </div><!-- .section-heading -->
                    <div class="event-wrap d-flex flex-wrap justify-content-between">
                        <figure class="m-0">
                            <img src="images/MARKET_WOMEN.jpeg" alt="">
                        </figure>

                        <div class="event-content-wrap">
                            <header class="entry-header d-flex flex-wrap align-items-center">
                                <h3 class="entry-title w-100 m-0"><a href="#">MARKET WOMEN FINANCIAL INCLUSION CAMPAIGN</a></h3>

                                <div class="posted-date">
                                    <a href="#">Feb,  2020</a>
                                </div><!-- .posted-date -->

                                <div class="cats-links">
                                    <a href="#">BENUE STATE</a>
                                </div><!-- .cats-links -->
                            </header><!-- .entry-header -->
                        </div><!-- .event-content-wrap -->
                    </div><!-- .event-wrap -->
                    
                    
                    <div class="event-wrap d-flex flex-wrap justify-content-between">
                        <figure class="m-0">
                            <img src="images/ORPHANAGE.jpeg" alt="">
                        </figure>

                        <div class="event-content-wrap">
                            <header class="entry-header d-flex flex-wrap align-items-center">
                                <h3 class="entry-title w-100 m-0"><a href="#">ORPHANAGE IMPACT IN REHOBOTH</a></h3>

                                <div class="posted-date">
                                    <a href="#">2019 </a>
                                </div><!-- .posted-date -->

                                <div class="cats-links">
                                    <a href="#">NAMIBIA</a>
                                </div><!-- .cats-links -->
                            </header><!-- .entry-header -->
                        </div><!-- .event-content-wrap -->
                    </div><!-- .event-wrap -->
                    <div class="event-wrap d-flex flex-wrap justify-content-between">
                        <figure class="m-0">
                            <img src="images/Talent.jpeg" alt="">
                        </figure>

                        <div class="event-content-wrap">
                            <header class="entry-header d-flex flex-wrap align-items-center">
                                <h3 class="entry-title w-100 m-0"><a href="#">Kids' Talent Show</a></h3>

                                <div class="posted-date">
                                    <a href="#">February 6-8, 2018</a>
                                </div><!-- .posted-date -->

                                <div class="cats-links">
                                    <a href="#">Namibia</a>
                                </div><!-- .cats-links -->
                            </header><!-- .entry-header -->
                        </div><!-- .event-content-wrap -->
                    </div><!-- .event-wrap -->
                </div><!-- .upcoming-events -->
            </div><!-- .col -->

            <div class="col-12 col-lg-6">
                <div class="featured-cause">
                    <div class="section-heading">
<!--                        <h2 class="entry-title">Featured Cause</h2>-->
                    </div><!-- .section-heading -->

                    <div class="cause-wrap d-flex flex-wrap justify-content-between">
                        <figure class="m-0">
                            <img src="images/COVID-19.jpeg" alt="">
                        </figure>

                        <div class="cause-content-wrap">
                            <header class="entry-header d-flex flex-wrap align-items-center">
                                <h3 class="entry-title w-100 m-0"><a href="#">COVID-19 PALLIATIVE DISBURSEMENT</a></h3>

                                <div class="posted-date">
                                    <a href="#">April 22, 2020</a>
                                </div><!-- .posted-date -->

                                <div class="cats-links">
                                    <a href="#">At Jikwoyi, Abuja</a>
                                </div><!-- .cats-links -->
                            </header><!-- .entry-header -->
                        </div><!-- .cause-content-wrap -->
                        
                    </div><!-- .cause-wrap -->
                </div><!-- .featured-cause -->
            </div><!-- .col -->
        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- .home-page-events -->

<div class="our-causes">
    <div class="container">
        <div class="row">
            <div class="coL-12">
                <div class="section-heading">
                    <h2 class="entry-title">Meet our team</h2>
                </div><!-- .section-heading -->
            </div><!-- .col -->
        </div><!-- .row -->

        <div class="row">
            <div class="col-12">
                <div class="swiper-container causes-slider">
                   <div class="swiper-wrapper">
                       <div class="swiper-slide">
                           <div class="cause-wrap">
                               <figure class="m-0">
                                   <img src="images/ceo.jpeg" alt="">
                               </figure>

                               <div class="cause-content-wrap">
                                   <header class="entry-header d-flex flex-wrap align-items-center">
                                       <h3 class="entry-title w-100 m-0"><a>Dr.MAGNUS ONYEAKU</a>
                                       </h3>
                                   </header><!-- .entry-header -->

                                   <div class="entry-content">
                                       <p class="m-0">Co-Founder/President</p>
                                   </div><!-- .entry-content -->
                               </div><!-- .cause-content-wrap -->
                           </div><!-- .cause-wrap -->
                       </div>
                       <div class="swiper-slide">
                           <div class="cause-wrap">
                               <figure class="m-0">
                                   <img src="images/vice.jpeg" alt="">
                               </figure>

                               <div class="cause-content-wrap">
                                   <header class="entry-header d-flex flex-wrap align-items-center">
                                       <h3 class="entry-title w-100 m-0"><a>CRISTEN ONYEAKU</a>
                                       </h3>
                                   </header><!-- .entry-header -->

                                   <div class="entry-content">
                                       <p class="m-0">Co-Founder/V.President</p>
                                   </div><!-- .entry-content -->
                               </div><!-- .cause-content-wrap -->
                           </div><!-- .cause-wrap -->
                       </div>
                       <div class="swiper-slide">
                           <div class="cause-wrap">
                               <figure class="m-0">
                                   <img src="images/vice2.jpeg" alt="">
                               </figure>

                               <div class="cause-content-wrap">
                                   <header class="entry-header d-flex flex-wrap align-items-center">
                                       <h3 class="entry-title w-100 m-0"><a>Amb.Godswill W.T Ogbari(PhD)</a>
                                       </h3>
                                   </header><!-- .entry-header -->

                                   <div class="entry-content">
                                       <p class="m-0">V.President Nigeria</p>
                                   </div><!-- .entry-content -->
                               </div><!-- .cause-content-wrap -->
                           </div><!-- .cause-wrap -->
                       </div>
                       <div class="swiper-slide">
                           <div class="cause-wrap">
                               <figure class="m-0">
                                   <img src="images/legal.jpg" alt="">
                               </figure>

                               <div class="cause-content-wrap">
                                   <header class="entry-header d-flex flex-wrap align-items-center">
                                       <h3 class="entry-title w-100 m-0"><a>Barr.Nkasiobi Atasie </a>
                                       </h3>
                                   </header><!-- .entry-header -->

                                   <div class="entry-content">
                                       <p class="m-0">Exec/Legal Secretary</p>
                                   </div><!-- .entry-content -->
                               </div><!-- .cause-content-wrap -->
                           </div><!-- .cause-wrap -->
                       </div>
                       <div class="swiper-slide">
                           <div class="cause-wrap">
                               <figure class="m-0">
                                   <img src="images/Protocol.jpeg" alt="">
                               </figure>

                               <div class="cause-content-wrap">
                                   <header class="entry-header d-flex flex-wrap align-items-center">
                                       <h3 class="entry-title w-100 m-0"><a>Dr. Ikeh Joseph</a>
                                       </h3>
                                   </header><!-- .entry-header -->

                                   <div class="entry-content">
                                       <p class="m-0">Protocol Officer.</p>
                                   </div><!-- .entry-content -->
                               </div><!-- .cause-content-wrap -->
                           </div><!-- .cause-wrap -->
                       </div>
                       <div class="swiper-slide">
                           <div class="cause-wrap">
                               <figure class="m-0">
                                   <img src="images/Nat.jpg" alt="">
                               </figure>

                               <div class="cause-content-wrap">
                                   <header class="entry-header d-flex flex-wrap align-items-center">
                                       <h3 class="entry-title w-100 m-0"><a>Pst.Jerry Nwankwora</a>
                                       </h3>
                                   </header><!-- .entry-header -->

                                   <div class="entry-content">
                                       <p class="m-0">Nat'l pro</p>
                                   </div><!-- .entry-content -->
                               </div><!-- .cause-content-wrap -->
                           </div><!-- .cause-wrap -->
                       </div>
                       <div class="swiper-slide">
                           <div class="cause-wrap">
                               <figure class="m-0">
                                   <img src="images/Secretary.jpeg" alt="">
                               </figure>

                               <div class="cause-content-wrap">
                                   <header class="entry-header d-flex flex-wrap align-items-center">
                                       <h3 class="entry-title w-100 m-0"><a>Amarachi Nwoke</a>
                                       </h3>
                                   </header><!-- .entry-header -->

                                   <div class="entry-content">
                                       <p class="m-0">Fin. Secretary</p>
                                   </div><!-- .entry-content -->
                               </div><!-- .cause-content-wrap -->
                           </div><!-- .cause-wrap -->
                       </div>
                   </div>
                </div><!-- .swiper-container -->

                <!-- Add Arrows -->
                <div class="swiper-button-next flex justify-content-center align-items-center">
                    <span><svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path
                                    d="M1171 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"/></svg></span>
                </div>

                <div class="swiper-button-prev flex justify-content-center align-items-center">
                    <span><svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path
                                    d="M1203 544q0 13-10 23l-393 393 393 393q10 10 10 23t-10 23l-50 50q-10 10-23 10t-23-10l-466-466q-10-10-10-23t10-23l466-466q10-10 23-10t23 10l50 50q10 10 10 23z"/></svg></span>
                </div>
            </div><!-- .col -->
        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- .our-causes -->

<div class="home-page-limestone">
    <div class="container">
        <div class="row align-items-end">
            <div class="coL-12 col-lg-12">
                <div class="section-heading">
                    <h2 class="entry-title">Our Vision</h2>
                    <p class="mt-5">TO RAISE AN ARMY OF SUSTAINABLE ENTERPRISES FROM AFRICA'S ECONOMIC DUNGHILLS.</p>
                    <p>By interpretation, our driving force is that salient potentials in Africans, most of which have receded into unconscious minds, can be re-awakened and turned into the successful entrepreneurs they are naturally meant to be. </a></p>
                </div><!-- .section-heading -->
                <div class="entry-footer mt-5">
                    <a href="our_v.php" class="btn gradient-bg mr-2">Read More</a>
                </div><!-- .entry-footer -->
            </div><!-- .col -->
        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- .our-causes -->


<?php include('./includes/footer.php') ?>
</body>
</html>
