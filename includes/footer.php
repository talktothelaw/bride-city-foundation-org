<footer class="site-footer">
  <div class="footer-widgets">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-6 col-lg-3">
          <div class="foot-about">
            <h2><a class="foot-logo" href="#"><img src="images/logo.png" style="height: 50px;" alt=""></a></h2>
            
            <p>Bride City Foundation is an NGO Microfinance Institution (MFI) which started operation as a Community Based Organization in 2006 with the name: Bride City (Bauchi) Multi-Purpose Cooperative Society Ltd.</p>
            
            <ul class="d-flex flex-wrap align-items-center">
              <li><a href="https://web.facebook.com/bridecity.mfi?__nodl"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            </ul>
          </div><!-- .foot-about -->
        </div><!-- .col -->
        
        <div class="col-12 col-md-6 col-lg-3 mt-5 mt-md-0">
          <h2>Useful Links</h2>
          
          <ul>
            <li><a href="/">Home</a></li>
            <li><a href="https://wealth-hub.org/validate.php">Become  a Volunteer</a></li>
            <li><a href="https://wealth-hub.org/donation.php">Donate</a></li>
          </ul>
        </div><!-- .col -->
        
        <div class="col-12 col-md-6 col-lg-3 mt-5 mt-md-0">
          <div class="foot-contact">
            <h2>Contact</h2>
            
            <ul>
              <li><i class="fa fa-phone"></i><span>+2347035301622</span></li>
              <li><i class="fa fa-envelope"></i><span>info@bridecity.org</span></li>
              <li><i class="fa fa-map-marker"></i><span>Suite 11,four line plaza phase 2 cele busstop, Jikwoyi Road, Old, Nigeria</span></li>
            </ul>
          </div><!-- .foot-contact -->
        </div><!-- .col -->
      </div><!-- .row -->
    </div><!-- .container -->
  </div><!-- .footer-widgets -->
  
  <div class="footer-bar">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <p class="m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved by <a href="https://visdeveloper.com" target="_blank">Visual Developers</a>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
        </div><!-- .col-12 -->
      </div><!-- .row -->
    </div><!-- .container -->
  </div><!-- .footer-bar -->
</footer><!-- .site-footer -->

<script type='text/javascript' src='js/jquery.js'></script>
<script type='text/javascript' src='js/jquery.collapsible.min.js'></script>
<script type='text/javascript' src='js/swiper.min.js'></script>
<script type='text/javascript' src='js/jquery.countdown.min.js'></script>
<script type='text/javascript' src='js/circle-progress.min.js'></script>
<script type='text/javascript' src='js/jquery.countTo.min.js'></script>
<script type='text/javascript' src='js/jquery.barfiller.js'></script>
<script type='text/javascript' src='js/custom.js'></script>
