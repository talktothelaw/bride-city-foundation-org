<header class="site-header">
  <div class="top-header-bar">
    <div class="container">
      <div class="row flex-wrap justify-content-center justify-content-lg-between align-items-lg-center">
        <div class="col-12 col-lg-8 d-none d-md-flex flex-wrap justify-content-center justify-content-lg-start mb-3 mb-lg-0">
          <div class="header-bar-email">
            MAIL: <a href="#">info@bridecity.org</a>
          </div><!-- .header-bar-email -->
          
          <div class="header-bar-text">
            <p>PHONE: <span>+2347035301622</span></p>
          </div><!-- .header-bar-text -->
        </div><!-- .col -->
        
        <div class="col-12 col-lg-4 d-flex flex-wrap justify-content-center justify-content-lg-end align-items-center">
          <div class="donate-btn">
            <a href="https://wealth-hub.org/donation.php">Donate Now</a>
          </div><!-- .donate-btn -->
        </div><!-- .col -->
      </div><!-- .row -->
    </div><!-- .container -->
  </div><!-- .top-header-bar -->
  
  <div class="nav-bar">
    <div class="container">
      <div class="row">
        <div class="col-12 d-flex flex-wrap justify-content-between align-items-center">
          <div class="site-branding d-flex align-items-center">
            <a class="d-block" href="index.php" rel="home"><img class="d-block" src="images/logo.png" height="50" alt="logo"></a>
          </div><!-- .site-branding -->
          
          <nav class="site-navigation d-flex justify-content-end align-items-center">
            <ul class="d-flex flex-column flex-lg-row justify-content-lg-end align-content-center">
              <li class="<?php echo $home ?>"><a href="index.php">Home</a></li>
              <li class="<?php echo $about ?>"><a href="about.php">About us</a></li>
              <li ><a href="https://wealth-hub.org/validate.php">Volunteer</a></li>
              <li><a href="https://wealth-hub.org/donation.php">Donate</a></li>
              <li  class="<?php echo $contact ?>"><a href="contact.php">Contact</a></li>
            </ul>
          </nav><!-- .site-navigation -->
          
          <div class="hamburger-menu d-lg-none">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
          </div><!-- .hamburger-menu -->
        </div><!-- .col -->
      </div><!-- .row -->
    </div><!-- .container -->
  </div><!-- .nav-bar -->
</header><!-- .site-header -->
