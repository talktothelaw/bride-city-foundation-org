<?php include('./includes/header.php') ?>
<body class="single-page contact-page">
<?php $contact = "current-menu-item"; ?>
<?php include('./includes/nav.php') ?>

<div class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>Contact</h1>
            </div><!-- .col -->
        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- .page-header -->

<div class="contact-page-wrap">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-5">
                <div class="entry-content">
                    <h2>Get In touch with us</h2>
                    <ul class="contact-info p-0">
                        <li><i class="fa fa-phone"></i><span>+2347035301622</span></li>
                        <li><i class="fa fa-envelope"></i><span>info@bridecity.com.ng</span></li>
                        <li><i class="fa fa-map-marker"></i><span>Suite 11,four line plaza phase 2 cele busstop, Jikwoyi Road, Old, Nigeria</span>
                        </li>
                    </ul>
                    <ul class="contact-social d-flex flex-wrap align-items-center">
                        <li><a href="https://web.facebook.com/bridecity.mfi?__nodl"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    </ul>
                </div>
            </div><!-- .col -->

            <div class="col-12 col-lg-7">
                <form class="contact-form">
                    <input type="text" placeholder="Name">
                    <input type="email" placeholder="Email">
                    <textarea rows="15" cols="6" placeholder="Messages"></textarea>

                    <span>
                            <input class="btn gradient-bg" type="submit" value="Contact us">
                        </span>
                </form><!-- .contact-form -->

            </div><!-- .col -->

            <div class="col-12">
                <div class="contact-gmap">
                    <iframe width="600" height="450" frameborder="0" style="border:0"
                            src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7881.843609341887!2d7.560263!3d8.979343!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa0506a01b393c1cc!2sBride%20City%20Cooperative!5e0!3m2!1sen!2sus!4v1575984587283!5m2!1sen!2sus"
                            allowfullscreen></iframe>
                </div>
            </div>
        </div><!-- .row -->
    </div><!-- .container -->
</div>

<?php include('./includes/footer.php') ?>
</body>
</html>
