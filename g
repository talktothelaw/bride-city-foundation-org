RewriteEngine on
RewriteCond %{HTTPS} off
RewriteCond %{HTTP:X-Forwarded-SSL} !on
RewriteCond %{HTTP_HOST} ^bridecity\.org$ [OR]
RewriteCond %{HTTP_HOST} ^www\.bridecity\.org$
RewriteRule ^(.*)$ "https\:\/\/bridecity\.org\/$1" [R=301,L]
