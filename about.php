<?php include('./includes/header.php') ?>
<?php $about = "current-menu-item"; ?>
<body class="single-page about-page">
<?php include('./includes/nav.php') ?>
<div class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>About Us</h1>
            </div><!-- .col -->
        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- .page-header -->

<div class="welcome-wrap">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-6 order-1 order-lg-2" >
                <img src="images/ceo.jpeg" style="max-width: 24em;" alt="welcome">
            </div><!-- .col -->
            <div class="col-12 col-sm-6 order-2 order-lg-1">
                <div class="welcome-content">
                    <header class="entry-header">
                        <h2 class="entry-title">Bride City Foundation</h2>
                    </header><!-- .entry-header -->

                    <div class="entry-content mt-lg-auto">
                        <p>Founded in 2006 by Dr. Magnus Onyeaku & Cristen Onyeaku, Bride City Foundation has developed from a CBO to a Multi-national NGO.
                            Bride City Foundation was Incorporated as an NGO in 2018 as CAC/IT/No 119368, in Nigeria. </p>
                    </div><!-- .entry-content -->
                   
                </div><!-- .welcome-content -->
                <p>Our primary areas of operation include: </p>
                <ul class="service_list">
                    <li>Child Protection</li>
                    <li>Gender Development</li>
                    <li>Youth Development</li>
                    <li>Capacity Building</li>
                    <li>Education</li>
                    <li>Food Security</li>
                    <li>Cooperative Development</li>
                    <li>Financial Inclusion</li>
                    <li>Climatic Change Management</li>
                </ul>
            </div><!-- .col -->

          
        </div><!-- .row -->
        <div class="entry-content mt-5">
            <h6>OPERATIONAL METHOD</h6>
            <p class="complete_about">The NGO works in partnership with its 5 Members' Cooperatives, technical companies and financial services companies. This helps it mobilize, validate and segment prospective beneficiaries ahead of every social and financial impact.</p>
            <p class="complete_about">The Cooperatives are led by some Volunteer Networks whose primary task is data mobilization, validation and classification into Cooperative Units and Clusters. The NGO offers Skill Acquisition and other Capacity Building Modules for effective Segmentation and, financial partners come in with grants and/or loan facilities.
            </p>
        </div>
        <div class="entry-content mt-5">
            <h5>MEMBERSHIP</h5>
            <p>Membership is of 4 categories:</p>
            <ol class="categories">
                <li>
                    <span>CHARITY: </span> For highly vulnerable and non-economic persons in need of food, shelter, clothing, education, healthcare, etc.
                </li>
                <li>
                    <span>BASIC: </span> For economic passive persons in need of economic activation. Inexperienced starters who need Skill Acquisition and Seed Capital. These need training and Capacity Development.
                </li>
                <li>
                    <span>STANDARD: </span> For economically active and experienced persons in need of Business Scale Support.
                </li>
                <li>
                    <span>ELITE: </span> For Volunteer Leaders. Though they need help as Standard Members do, they have developed their leader skills to mobilize, validate, process and coordinate member data for various Coops of the NGO.
                </li>
            </ol>
        </div>
        <div class="entry-content mt-5">
            <h5>PROFILE</h5>
            <p>Bride City Foundation is a member of the association of Non-Governmental Organizations of Nigeria (ANGON). It is licensed for Microfinance and Financial Inclusion operations under the Association of Non-bank Microfinance Institutions of Nigeria (ANMFIN), through which it makes its monthly rendition to the Central Bank of Nigeria. </p>
            <p>The NGO has a working relationship with the Nigerian Inter-Bank Settlement System (NIBSS) as a BVN Enrollment Institution. It is also in working relationships with Capital Express Assurance, Allianz Insurance, Interswitch and IFAD/FGN VCDP in Minna, Nigeria.</p>
        </div>
    </div><!-- .container -->
</div><!-- .home-page-icon-boxes -->



<div class="help-us">
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex flex-wrap justify-content-between align-items-center">
                <h2>Help us so we can help others</h2>

                <a class="btn orange-border" href="https://wealth-hub.org/donation.php">Donate now</a>
            </div>
        </div>
    </div>
</div>

<?php include('./includes/footer.php') ?>
</body>
</html>
