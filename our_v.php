<?php include('./includes/header.php') ?>
<body class="single-page about-page">
<?php include('./includes/nav.php') ?>
<div class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>
                    Bride City Foundation
                </h1>
            </div><!-- .col -->
        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- .page-header -->

<div class="welcome-wrap">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 order-1 order-lg-1">
                <div class="welcome-content">
                    <header class="entry-header">
                        <h6 class="entry-title">OUR VISION</h6>
                    </header><!-- .entry-header -->

                    <div class="entry-content mt-lg-auto">
                        <p>
                            TO RAISE AN ARMY OF SUSTAINABLE ENTERPRISES FROM AFRICA'S ECONOMIC DUNGHILLS.
                        </p>
                        <p>
                            By interpretation, our driving force is that salient potentials in Africans, most of which have receded into unconscious minds, can be re-awakened and turned into the successful entrepreneurs they are naturally meant to be.
                        </p>
                    </div><!-- .entry-content -->
                    <div class="entry-content mt-lg-auto">
                        <header class="entry-header">
                            <h6 class="entry-title">OUR MISSION</h6>
                        </header><!-- .entry-header -->
                        <p>
                            TO ACCELERATE SUSTAINABLE DEVELOPMENT THROUGH CAPACITY BUILDING AND FINANCIAL INCLUSION.
                        </p>
                        We build our members' financial capacity and make every member group investment ready for scale funding.
                        We cannot stop until our most vulnerable member is financially set to build and sustain generational Wealth.
                    </div><!-- .entry-content -->

                </div><!-- .welcome-content -->
                <br/>
                <p>CORE VALUES: </p>
                <ol class="service_list service_list_two">
                    <li>Social and Financial Integrity</li>
                    <li>To rid Africans of SOCIO-ECONOMIC MYOPIA which has made many unable to plan for future generations.</li>
                    <li>Cooperative Effort and Corporate Synergy. These mould  our vehicle to grow the largest Economic communities in Africa, where all members shall be stakeholders and shareholders.</li>
                    <li> Business Innovation.</li>
                </ol>
            </div><!-- .col -->
        </div><!-- .row -->
        <br/>
        <strong>BrideCity</strong>
    </div><!-- .container -->
</div><!-- .home-page-icon-boxes -->



<div class="help-us">
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex flex-wrap justify-content-between align-items-center">
                <h2>Help us so we can help others</h2>

                <a class="btn orange-border" href="https://wealth-hub.org/donation.php">Donate now</a>
            </div>
        </div>
    </div>
</div>

<?php include('./includes/footer.php') ?>
</body>
</html>
